FROM eu.gcr.io/cloudsql-docker/gce-proxy:1.37.3-alpine   as gcp
FROM ghcr.io/amacneil/dbmate:2.24.2

# hadolint ignore=DL3018
RUN apk add --no-cache \
  openssh-client \
  git \
  bash \
  libc6-compat
COPY --from=gcp /cloud_sql_proxy /usr/local/bin/

ENTRYPOINT []